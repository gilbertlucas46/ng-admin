
# Based on Angular 4+, Bootstrap 4

### What's included:

- Angular 4+ & Typescript
- Bootstrap 4+ & SCSS
- Responsive layout
# How to develop
- Clone ng-admin: https://gilbertlucas46@bitbucket.org/gilbertlucas46/ng-admin.git
- Run **npm install**
- Run **npm start** then open http://localhost:4200

