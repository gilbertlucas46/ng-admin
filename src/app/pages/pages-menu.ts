import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [

  {
    title: 'Signups',
    icon: 'fa fa-users',
      link: '/pages/tables/smart-table',
      home: true,

  },
];
